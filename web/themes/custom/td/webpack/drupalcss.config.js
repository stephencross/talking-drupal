const storybook = process.env.STORYBOOK || false;
const storybookStatic = process.env.STORYBOOKSTATIC || false;

const cdnHost = "";
var cdn = "";
var cssPath = "css/";

if (storybook) {
  cssPath = `css/`;
  cdn = "";
} else if (storybookStatic) {
  cssPath = `css/`;
  cdn = `${cdnHost}/latest/assets`;
} else {
  cdn = `/themes/custom/td/public`;
  cssPath = `/`;
}

const compiledCss = async () => {
  return {
    mode: "production",
    entry: [__dirname + "/../sass/styles.scss"],
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [
            {
              loader: "file-loader",
              options: { outputPath: cssPath, name: "tdStyles.css" },
            },
            "postcss-loader",
            {
              loader: "sass-loader",
              options: {
                additionalData: `$cdn: "${cdn}";`,
              },
            },
          ],
        },
      ],
    },
  };
};

const fullBuild = async () => {
  var builds = [];
  builds.push(await compiledCss());

  return builds;
};

module.exports = async () => {
  return await fullBuild();
};
