echo "Status"
ddev drush ms
echo "Reset Migrations"
ddev drush mrs td_audios
ddev drush mrs td_thumbnails
ddev drush mrs td_videos
ddev drush mrs td_hosts
ddev drush mrs td_hosts_2
ddev drush mrs td_episodes
echo "Rolling back migration"
ddev drush mr --all
echo "Reinstalling module"
ddev drush pmu talkingdrupal_migration
ddev drush en talkingdrupal_migration
echo "Running td_audios migration"
ddev drush mim td_audios
echo "Running td_thumbnails migration"
ddev drush mim td_thumbnails
echo "Running td_videos migration"
ddev drush mim td_videos
echo "Running td_hosts migration"
ddev drush mim td_hosts
echo "Running td_hosts_2 migration"
ddev drush mim td_hosts_2
echo "Running td_episodes migration"
ddev drush mim td_episodes
echo "Status"
ddev drush ms