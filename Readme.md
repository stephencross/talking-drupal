## Local Development - First time setup

- Install ddev - https://ddev.readthedocs.io/en/stable/users/install/ddev-installation/
- Clone repo - `git clone git@gitlab.com:stephencross/talking-drupal.git`
- Start ddev  - `ddev start`
- Install npm - `ddev bi`
- Build theme - `ddev bt`
- Refresh database - `ddev refresh`

## DDEV Commands

- ddev bi - install node
- ddev bt - build theme
- ddev refresh - downloads production database, run updb, config import, copy files from production and generate at ULI

DDEV Commands not yet working - codefix, codesniff, stand and unit.

## Local Settings

$settings['file_private_path'] = 'sites/default/files/private';
