#!/bin/bash

## Description: phpstan
## Usage: stan
## Example: stan

./vendor/bin/phpstan --configuration=phpstan.neon analyse \
  web/themes/custom/td \
  web/modules/custom