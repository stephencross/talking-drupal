#!/bin/bash

## Description: Sniff code
## Usage: cds
## Example: cds

./vendor/bin/phpcs \
  --standard="Drupal,DrupalPractice" -n \
  --extensions="php,module,inc,install,test,profile,theme" \
  --ignore="web/themes/custom/td/node_modules" \
  web/themes/custom/td \
  web/modules/custom