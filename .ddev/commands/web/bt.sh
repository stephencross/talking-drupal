#!/bin/bash

## Description: build theme
## Usage: bt
## Example: bt

cd web/themes/custom/td
npm run build-css
drush cr
